from django.contrib.auth.models import User
from django.db import models
from django.template.defaultfilters import truncatewords


class Post(models.Model):
    title = models.CharField(max_length=75, blank=True)
    text_body = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ("-created",)

    def __unicode__(self):
        return self.admin_display_lead

    @property
    def admin_display_lead(self):
        if self.title:
            return self.title

        return truncatewords(self.text_body, "10")

    @property
    def lead_image_url(self):
        lead_image = self.postimage_set.filter(lead_image=True).first()

        if lead_image:
            return lead_image.image_url

        return ''

    def does_user_like(self, user):
        if user.is_authenticated():
            return PostLike.objects.filter(user=user, post=self).exists()

        return False


class PostImage(models.Model):
    post = models.ForeignKey(Post)
    image_url = models.URLField()
    caption = models.TextField(blank=True)
    lead_image = models.BooleanField(default=False)

    def __unicode__(self):
        return "Image for post id %s" % self.post.id


class PostLike(models.Model):
    user = models.ForeignKey(User)
    post = models.ForeignKey(Post)

    class Meta:
        unique_together = ("user", "post")
