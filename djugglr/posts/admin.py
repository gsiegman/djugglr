from django.contrib import admin

from .models import Post, PostImage, PostLike


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ("admin_display_lead", "created",)


@admin.register(PostImage)
class PostImageAdmin(admin.ModelAdmin):
    pass


@admin.register(PostLike)
class PostLikeAdmin(admin.ModelAdmin):
    pass
