import json

from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.template.defaultfilters import linebreaks_filter
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST

from .models import Post, PostLike


def posts_index(request):
    posts_qs = Post.objects.all()
    posts = [{
        "id": post.id,
        "text_body": linebreaks_filter(post.text_body),
        "title": post.title,
        "lead_image_url": post.lead_image_url,
        "liked": post.does_user_like(request.user),
        "created": post.created.strftime("%c %Z"),
    } for post in posts_qs]

    props = json.dumps({
        "posts": posts
    })

    return render_to_response("index.html", {
        "module": "./src/pages/DjugglrIndex",
        "props": props,
    }, context_instance=RequestContext(request))


def post_detail(request, pk):
    post_obj = get_object_or_404(Post, pk=pk)
    post = {
        "id": post_obj.id,
        "text_body": linebreaks_filter(post_obj.text_body),
        "title": post_obj.title,
        "lead_image_url": post_obj.lead_image_url,
        "liked": post_obj.does_user_like(request.user),
        "created": post_obj.created.strftime("%c %Z"),
    }

    props = json.dumps(post)

    return render_to_response("index.html", {
        "module": "./src/pages/DjugglrPostDetail",
        "props": props,
    }, context_instance=RequestContext(request))


@csrf_exempt
@require_POST
def like_toggle(request, pk):
    if request.user.is_authenticated():
        post = get_object_or_404(Post, pk=pk)
        user = request.user

        like, created = PostLike.objects.get_or_create(post=post, user=user)

        if not created:
            like.delete()

        return HttpResponse()

    return HttpResponseForbidden()
