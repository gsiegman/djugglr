from django import template

from django_react_utils.render_api_client import get_rendered_component


register = template.Library()


@register.simple_tag
def react_component(module, props=None):
    return get_rendered_component(module, props)
