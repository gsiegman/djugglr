import requests

from django.conf import settings


def get_rendered_component(module, props=None):
    params = {
        "module": module,
        "props": props,
    }

    rc = requests.get(settings.REACT_COMPONENT_ENDPOINT, params=params)

    return rc.content
