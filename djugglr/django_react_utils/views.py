import json

from django.core.exceptions import ImproperlyConfigured
from django.views.generic import TemplateView


class ReactTemplateView(TemplateView):
    module = None
    props = {}
    state = []

    def get_context_data(self, **kwargs):
        context = super(ReactTemplateView, self).get_context_data(**kwargs)

        context["module"] = self.get_module()
        context["props"] = self.get_props()
        context["state"] = self.get_state()

        return context

    def get_module(self):
        if self.module is None:
            raise ImproperlyConfigured(
                "ReactTemplateView requires a React module to be specified."
            )
        else:
            return self.module

    def get_props(self):
        props = json.dumps(self.props)
        return props

    def get_state(self):
        state = json.dumps(self.state)
        return state
