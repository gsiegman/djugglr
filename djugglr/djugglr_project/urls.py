from django.conf.urls import patterns, include, url
from django.contrib import admin


urlpatterns = patterns('',
    url(
        r'^$',
        'posts.views.posts_index',
        name='site_index'
    ),
    url(
        r'^posts/(?P<pk>\d+)/$',
        'posts.views.post_detail',
        name='posts_post_detail'
    ),
    url(
        r'^posts/(?P<pk>\d+)/toggle-like/$',
        'posts.views.like_toggle',
        name='posts_like_toggle',
    ),
    url(r'^admin/', include(admin.site.urls)),
)
