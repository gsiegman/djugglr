/** @jsx React.DOM */

var React = require('react');


var Header = React.createClass({

    render: function() {
        return (
            <header>
                <h1><a href="/">Glenn Siegman</a></h1>
                <p className="bio">
                Software architect. Human being.<br/>I love wine. Praise Jibbers. NYC.
                </p>
                <address>
                    Phone: <a href="tel:2026707434">202.670.7434</a><br/>
                    Email: <a href="mailto:gsiegman@gsiegman.com">gsiegman@gsiegman.com</a><br/>
                    Twitter: <a href="http://twitter.com/gsiegman/">@gsiegman</a><br/>
                    LinkedIn: <a href="http://www.linkedin.com/in/gsiegman">gsiegman</a>
                </address>
            </header>
        );
    }
});

module.exports = Header;
