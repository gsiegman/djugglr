/** @jsx React.DOM */

var React = require('react');
var DjugglrPost = require('./DjugglrPost');


var DjugglrPosts = React.createClass({

    render: function() {
        var content = this.props.posts.map(function(post) {
            return (
                <DjugglrPost key={post.id} post={post} />
            )
        });

        return (
            <section className='djugglr-posts'>{content}</section>
        )
    }

});


module.exports = DjugglrPosts;
