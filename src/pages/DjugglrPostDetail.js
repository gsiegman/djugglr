/** @jsx React.DOM */

var React = require('react');
var Grid = require('react-bootstrap/Grid');
var Row = require('react-bootstrap/Row');
var Col = require('react-bootstrap/Col');
var Header = require('../Header');
var DjugglrPost = require('../DjugglrPost');


var DjugglrPostDetail = React.createClass({

    getInitialState: function(props) {
        props = props || this.props;

        return {
            "post": props
        };
    },

    render: function() {
        return (
            <div className="djugglr-app">
                <Grid>
                    <Row>
                        <Col sm={3}>
                            <Header />
                        </Col>
                        <Col sm={7}>
                            <DjugglrPost key={this.state.post.id} post={this.state.post} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

});

module.exports = DjugglrPostDetail;
