/** @jsx React.DOM */

var React = require('react');
var Grid = require('react-bootstrap/Grid');
var Row = require('react-bootstrap/Row');
var Col = require('react-bootstrap/Col');
var Header = require('../Header');
var DjugglrPosts = require('../DjugglrPosts');


var DjugglrIndex = React.createClass({

    getInitialState: function(props) {
        props = props || this.props;

        return {
            "posts": props.posts
        };
    },

    render: function() {
        return (
            <div className="djugglr-app">
                <Grid>
                    <Row>
                        <Col sm={3}>
                            <Header />
                        </Col>
                        <Col sm={7}>
                            <DjugglrPosts posts={this.state.posts} />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

});

module.exports = DjugglrIndex;
