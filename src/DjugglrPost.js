/** @jsx React.DOM */

var React = require('react');
var Timestamp = require('react-time');
var LikeButton = require('./LikeButton');


var DjugglrPost = React.createClass({

    render: function() {
        var post = this.props.post;
        var postDate = new Date(post.created);
        var postURL = '/posts/' + post.id + '/';
        var postLiked = post.liked;

        if (post.title) {
            postTitle = <h1 className='djugglr-post-title'>{post.title}</h1>;
        } else {
            var postTitleHolderStyle = {
                paddingTop: '20px',
                paddingBottom: '10px'
            }
            postTitle = <div style={postTitleHolderStyle}></div>;
        }

        if (post.lead_image_url) {
            postLeadImageURL = <img src={post.lead_image_url} className="img-responsive djugglr-post-image" />
        } else {
            postLeadImageURL = '';
        }

        return (
            <article className='djugglr-post'>
                {postTitle}
                {postLeadImageURL}
                <div dangerouslySetInnerHTML={{__html: post.text_body}} />
                <LikeButton postID={post.id} liked={postLiked} />
                <a href={postURL}><Timestamp className='djugglr-post-timestamp' value={postDate} relative /></a>
            </article>
        )
    }

});

module.exports = DjugglrPost;
