/** @jsx React.DOM */

var React = require('react');
var Button = require('react-bootstrap/Button');
var request = require('superagent');


var LikeButton = React.createClass({

    getInitialState: function() {
        return {liked: this.props.liked, postID: this.props.postID};
    },

    handleClick: function(event) {
        var toggleLikeURL = '/posts/' + this.state.postID + '/toggle-like/';
        var that = this;

        request.post(toggleLikeURL)
            .end(function(err, res) {
                if (res.error) {
                    alert("You must be logged in to like a post");
                } else {
                    that.setState({liked: !that.state.liked});
                }
            });
    },

    render: function() {
        var text = this.state.liked ? 'Unlike' : 'Like';
        return (
            <div>
                <Button onClick={this.handleClick} bsStyle='primary'>{text}</Button>
            </div>
        )
    }
});

module.exports = LikeButton;
